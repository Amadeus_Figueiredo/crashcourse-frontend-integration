package tesolva.crashcourse.hellocontroller;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class HelloControllerTest {

    @Test
    void hello() {
        final HelloController helloController = new HelloController();
        final String result = helloController.hello("Sitting Bull");
        Assertions.assertThat(result).isEqualTo("Hello, Sitting Bull!");
    }
}