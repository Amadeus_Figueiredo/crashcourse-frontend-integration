//package tesolva.crashcourse.todos;
//
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.stereotype.Service;
//
//import java.util.List;
//
//@Service
//public class TodoService {
//
//    private final TodoRepository todoRepository;
//
//    public TodoService(TodoRepository todoRepository) {
//        this.todoRepository = todoRepository;
//    }
//
//    public List<Todo> getAllSavedTodos(String username) {
//        return todoRepository.findByUsername(username);
//    }
//
//    public Todo getOneTodo(long id) {
//        return todoRepository.findById(id).get();
//    }
//
//    public ResponseEntity<Void> deleteOneTodo(long id) {
//        todoRepository.deleteById(id);
//        return ResponseEntity.noContent().build();
//    }
//
//    public Todo updateOneTodo(Todo todo) {
//        return todoRepository.save(todo);
//    }
//
//    public Todo createOneTodo(Todo todo) {
//        return todoRepository.save(todo);
//    }
//
//}
