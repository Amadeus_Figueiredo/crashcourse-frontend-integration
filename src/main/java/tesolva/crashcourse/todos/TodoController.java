package tesolva.crashcourse.todos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;


@CrossOrigin("http://localhost:3000")
@RestController
public class TodoController {

    @Autowired
    private TodoRepository todoRepository;

    @GetMapping("/jpa/users/{username}/todos")
    public List<Todo> getAllTodos(
            @PathVariable String username){
        return todoRepository.findByUsername(username);
    }
    // add .get() because todoJpaRepository returns optional
    @GetMapping("/jpa/users/{username}/todos/{id}")
    public Todo getTodo(
            @PathVariable String username,
            @PathVariable long id) {
        return todoRepository.findById(id).get();
    }

    @DeleteMapping("/jpa/users/{username}/todos/{id}")
    public ResponseEntity<Void> deleteTodo(
            @PathVariable String username,
            @PathVariable long id){
        todoRepository.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @PutMapping("/jpa/users/{username}/todos/{id}")
    public ResponseEntity<Todo> updateTodo(
            @PathVariable String username,
            @PathVariable long id,
            @RequestBody Todo todo) {
        // because we do not populate the username field in the frontend we set it now in setUsername(username)
        todo.setUsername(username);
        Todo todoUpdated = todoRepository.save(todo);
        return new ResponseEntity<Todo>(todo, HttpStatus.OK);
    }

    @PostMapping("/jpa/users/{username}/todos")
    public ResponseEntity<Todo> createTodo(
            @PathVariable String username,
            @RequestBody Todo todo) {
        // // because we do not populate the username field in the frontend we set it now in setUsername(username)
        todo.setUsername(username);
        Todo createdTodo = todoRepository.save(todo);

        //To create we want to know the location of it, we want to get the id also compose the path of the new element
        // we are taking the current request path and appending /id and creating an url
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}").buildAndExpand(createdTodo.getId()).toUri();

        return ResponseEntity.created(uri).build();
    }
}