import React, {Component} from "react";
import {Link} from "react-router-dom"
import "../css/HeaderComponent.css";

class HeaderComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }

    render() {
        return (
            <>
                <div className="header-container">
                    <header>
                        <nav className='navbar'>
                            <a href="https://academy.tesolva.dev/crashcourse/"  className="logo" target="_blank"  rel="noopener noreferrer"><h3>TeSolva's Crashcourse</h3></a>
                            <ul className="nav-links">
                                <li><Link className="nav-item" to="/">Home</Link></li>
                                <li><Link className="nav-item" to="/todos">Todos</Link></li>
                            </ul>
                        </nav>
                    </header>
                </div>
            </>
        )
    }
}

export default HeaderComponent;
