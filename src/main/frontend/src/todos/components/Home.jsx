import React, {Component} from "react"
import '../css/HomeComponent.css'
import {Link} from "react-router-dom";

class HomeComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: "New User"
        }
    }

    render() {
        return (
            <>
                <div className="home-container">
                    <div>
                        <h4>Welcome {this.state.username}!<br/><br/>
                            Thanks for being there, in this journey with us.<br/><br/>
                            We want to bring together a backend with Springboot and a frontend with React with this To-Do App.<br/><br/>
                            The idea is to go through the steps - Entity, Controller and Repository<br/>
                            also using JPA, and Postgres.
                            Please click <Link to="/todos">here</Link> or on the Todos link in the header to see your list.
                        </h4>
                    </div>
                </div>
            </>
        )
    }
}

export default HomeComponent;