import React, {Component} from "react";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import FooterComponent from "./FooterComponent"
import HeaderComponent from "./HeaderComponent";
import HomeComponent from "./HomeComponent";
import TodoListComponent from "./TodoListComponent";
import '../css/TodoApp.css';
import TodoComponent from "./TodoComponent";

class TodoApp extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    render() {
        return (

                <div className="container">
                    <Router>
                        <>
                            <HeaderComponent/>
                            <Switch>
                                <Route path="/" exact component={HomeComponent}/>
                                <Route path="/todos" exact component={TodoListComponent}/>
                                <Route path="/todos/:id" exact component={TodoComponent}/>
                            </Switch>
                        </>
                    </Router>
                    <FooterComponent/>
                </div>

        )
    }
}

export default TodoApp;
