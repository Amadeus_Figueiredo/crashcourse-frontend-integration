import React, {Component} from "react"
import moment from "moment";
import {Form, Field, Formik, ErrorMessage} from "formik";
import "../css/TodoComponent.css"

import TodoDataService from "../api/TodoDataService";

class TodoComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: this.props.match.params.id,
            description: '',
            targetDate: moment(new Date()).format('YYYY-MM-DD'),
            done: false
        }
        this.onSubmit = this.onSubmit.bind(this)
        this.validate = this.validate.bind(this)
    }

    // we implement api calls inside of componentDidMount
    componentDidMount() {
        if (this.state.id === -1) {
            return
        }

        let username = "current-user";
        TodoDataService.retrieveTodo(username, this.state.id)
            .then(response => this.setState({
                description: response.data.description,
                targetDate: moment(response.data.targetDate).format('YYYY-MM-DD'),
                done: response.data.done
            }))
    }

    validate(values) {
        let errors = {}
        if (!values.description) {
            errors.description = 'Please, insert a description'
        } else if (values.description.length < 5) {
            errors.description = 'Please, insert at least 5 characters in description'
        }

        if (!moment(values.targetDate).isValid()) {
            errors.targetDate = 'Please, insert a valid date'
        }

        console.log(values)
        return errors;
    }

    onSubmit(values) {
        let username = "current-user";
        let todo = {
            id: this.state.id,
            description: values.description,
            targetDate: values.targetDate,
            done: values.done
        }
        if (this.state.id === -1) {
            TodoDataService.createTodo(
                username, todo)
                .then(() => {
                        this.props.history.push('/todos');
                    }
                )
        } else {
            TodoDataService.updateTodo(
                username, this.state.id, todo)
                .then(() => {
                        this.props.history.push('/todos');
                    }
                )
        }
    }

    render() {
        // can do it also with destructuring - let {description, targetDate} = this.state
        let description = this.state.description
        let targetDate = this.state.targetDate
        let done = this.state.done
        return (
            <div className="">
                <div className="">
                    <Formik
                        initialValues={{
                            // if key has the same name as the value you can write just - description instead of description: description
                            description: description,
                            targetDate: targetDate,
                            done: false
                        }}
                        onSubmit={this.onSubmit}
                        validate={this.validate}
                        validateOnBlur={false}
                        validateOnChange={false}
                        enableReinitialize={true}
                    >
                        {
                            (props) => (
                                <div className="">
                                    <ErrorMessage name="description" component="div"
                                                  className="alert alert-warning "></ErrorMessage>
                                    <ErrorMessage name="targetDate" component="div"
                                                  className="alert alert-warning "></ErrorMessage>
                                    <Form className="todo-table">

                                        <fieldset className="fieldset-xl">
                                            <label className="border"></label>
                                            <Field className="field-xl" type="text" name="description" placeholder="Enter description"/>
                                        </fieldset>
                                        <fieldset className="fieldset-m">
                                            <label className="border">Date</label>
                                            <Field className="field-m" type="date" name="targetDate"/>
                                        </fieldset>
                                        <fieldset className="fieldset-s">
                                            <label className="border">Done</label>
                                            <Field className="field-s" type="checkbox" name="done"/>
                                        </fieldset>
                                        <button className="btn-save" type="submit">Save</button>
                                    </Form>
                                </div>
                            )
                        }
                    </Formik>

                </div>
            </div>
        )
    }
}

export default TodoComponent;