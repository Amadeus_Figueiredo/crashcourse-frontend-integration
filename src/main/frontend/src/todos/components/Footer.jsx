import React, {Component} from "react";
import "../css/footer.css"

class FooterComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    render() {
        return (
            <>
                <div className="footer-container">
                    <footer>
                        <a href="https://www.tesolva.de/"  className="logo" target="_blank"  rel="noopener noreferrer"><span>©2022 by TeSolva Falk & Hattemer GbR</span></a>
                    </footer>
                </div>
            </>
        )
    }
}

export default FooterComponent;

