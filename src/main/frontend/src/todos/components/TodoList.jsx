import React, {Component} from "react"
import moment from "moment";
import "../css/TodoListComponent.css"

import TodoDataService from "../api/TodoDataService";

class TodoListComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            todos: []
        }
        this.addTodo = this.addTodo.bind(this);
        this.updateTodo = this.updateTodo.bind(this);
        this.deleteTodo = this.deleteTodo.bind(this);
        this.refreshTodos = this.refreshTodos.bind(this);
    }

    addTodo() {
        this.props.history.push(`/todos/-1`)
        console.log(this.state.todos)
    }

    updateTodo(id) {
        this.props.history.push(`/todos/${id}`)
        console.log(id);
        console.log(this.props);
    }

    deleteTodo(id) {
        let username = "current-user";
        TodoDataService.deleteTodo(username,id)
            .then(response => {
                this.setState({deleteMessage: `Delete of item ${id} by user ${username} successful`})
                    this.refreshTodos();
            })
    }

    refreshTodos() {
        let username = "current-user";

        TodoDataService.retrieveAllTodos(username)
            .then(response => {
                this.setState({todos: response.data})
            })

    }

    componentDidMount() {
        this.refreshTodos();
    }


    render() {
        return (
            <>
                <div className="container todo-list-container">
                    {this.state.deleteMessage && <div className="alert alert-success">{this.state.deleteMessage}</div>}
                    <table className="todo-list-table">
                        <thead className="todo-list-thead">
                        <tr>
                            <th>What</th>
                            <th>When</th>
                            <th>Done</th>
                            <th>Update</th>
                            <th>Delete</th>
                        </tr>
                        </thead>
                        <tbody className="todo-list-tbody">
                        {
                            this.state.todos.map(
                                todo =>
                                    <tr className="table-row" key={todo.id}>
                                        <td className="table-cell col-xl">{todo.description}</td>
                                        <td className="table-cell col-s">{moment(todo.targetDate).format("ll")}</td>
                                        <td className="table-cell col-s">{todo.done ? <h4>Yes</h4> : <h4>No</h4>} </td>
                                        <td className="table-cell col-s"><button className="update-todo-btn" onClick={() => this.updateTodo(todo.id)}>Update</button></td>
                                        <td className="table-cell col-s"><button className="delete-todo-btn" onClick={() => this.deleteTodo(todo.id)}>Delete</button></td>
                                        <hr/>
                                    </tr>
                            )
                        }
                        </tbody>
                    </table>
                    <button className="add-todo-btn" onClick={() => this.addTodo()}>Add to-do</button>
                </div>
            </>
        )
    }
}

export default TodoListComponent;