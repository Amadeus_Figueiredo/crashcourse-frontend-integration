import React from 'react';
import ReactDOM from 'react-dom'
import TodoApp from "./todos/components/TodoApp";
import './index.css';



ReactDOM.render(
    <TodoApp/>,
    document.getElementById("root")
);

